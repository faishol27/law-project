from locust import HttpUser, task, between

class LoadTestDB(HttpUser):
    wait_time = between(1, 3)

    @task
    def get_items(self):
        self.client.get('/api/v1/books?per_page=100')

    @task
    def update_items(self):
        self.client.put('/api/v1/books/1', json={'title': 'Buku', 'author': 'Siapapun', 'Sinopsis': 'A' * 200})