from flask import g
import redis
import os

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379")

def get_redis():
    r = getattr(g, '_redis', None)
    if r is None:
        r = g._redis = redis.from_url(REDIS_URL)
    return r

def get_key(k):
    r = get_redis()
    return r.get(k)

def set_key(k, v, ttl = 300):
    r = get_redis()
    r.set(k, v, ttl)

def del_key(k):
    r = get_redis()
    r.delete(k)