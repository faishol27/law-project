from datetime import datetime
from fastapi import FastAPI, HTTPException
from lib.db import Database
from lib.serializers import MahasiswaSerializer
import random

app = FastAPI()
db = Database()

@app.get("/read/{npm}")
def get_mahasiswa(npm):
    data = db.query_db("SELECT * FROM mahasiswa where npm = ?", [npm], one=True)
    if not data:
        raise HTTPException(status_code=404, detail="Mahasiswa not found")
    return {'status': 'OK', **MahasiswaSerializer().serialize(data)[0]}    

@app.get("/read/{npm}/{trxid}")
def get_mahasiswa_trxid(npm, trxid):
    data = db.query_db("SELECT * FROM mahasiswa where npm = ?", [npm], one=True)
    if not data:
        raise HTTPException(status_code=404, detail="Mahasiswa not found")
    return {'status': 'OK', 'trxid': trxid, 'random': random.randint(0, int(trxid)), 'time': datetime.now()}

@app.on_event("shutdown")
def shutdown_event():
    db.destroy()