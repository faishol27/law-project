from fastapi import FastAPI
from lib.db import Database
from lib.serializers import MahasiswaSerializer
from pydantic import BaseModel

class Mahasiswa(BaseModel):
    npm: str
    nama: str

app = FastAPI()
db = Database()

@app.post("/update")
def get_mahasiswa(data: Mahasiswa):
    data = db.query_db("INSERT INTO mahasiswa(npm, nama) VALUES(?,?)", [data.npm, data.nama])
    mhs_id = db.query_db("SELECT last_insert_rowid()", one = True)[0]
    data = db.query_db("SELECT * FROM mahasiswa WHERE id = ?", [mhs_id], one=True)
    return {'status': 'ok', 'data': MahasiswaSerializer().serialize(data)}

@app.on_event("shutdown")
def shutdown_event():
    db.destroy()