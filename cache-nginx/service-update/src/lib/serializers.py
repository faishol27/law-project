class MahasiswaSerializer():
    def __serial_single_row(self, row):
        return {
            "npm": row[1],
            "nama": row[2],
        }

    def serialize(self, data):
        ret = list()
        if len(data) == 0: return ret
        if type(data[0]) != type(list()):
            ret.append(self.__serial_single_row(data))
        else:
            for row in data:
                ret.append(self.__serial_single_row(row))
        return ret