from io import StringIO
from fastapi import FastAPI, HTTPException, UploadFile
from lib.db import Database
from lib.serializers import BookSerializer
from pydantic import BaseModel
from typing import Optional
from os import getenv
import csv

class Book(BaseModel):
    title: str
    author: str
    sinopsis: Optional[str] = ""

app = FastAPI()
db = Database()
UPLOAD_DIR = getenv("UPLOAD_DIR", "./uploads")

@app.get("/api/v1/books")
def get_books(page: int = 1, per_page: int = 10):
    skip = (page - 1) * per_page
    print(skip, per_page)
    data = db.query_db("SELECT * FROM books")
    print(data)
    return {'status': 'ok', 'data': BookSerializer().serialize(data)}    

@app.post("/api/v1/books")
def create_books(book: Book):
    data = db.query_db("INSERT INTO books(title, author, sinopsis) VALUES(?,?,?)", [book.title, book.author, book.sinopsis])
    book_id = db.query_db("SELECT last_insert_rowid()", one = True)[0]
    data = db.query_db("SELECT * FROM books WHERE id = ?", [book_id], one=True)
    return {'status': 'ok', 'data': BookSerializer().serialize(data)}

@app.put("/api/v1/books/{book_id}")
def update_books(book_id, book: Book):
    data = db.query_db("SELECT * FROM books WHERE id = ?", [book_id], one=True)
    if not data:
        raise HTTPException(status_code=404, detail="Book not found")
    db.query_db("UPDATE books SET title = ?, author = ?, sinopsis = ? WHERE id = ?", [book.title, book.author, book.sinopsis, book_id])
    data = db.query_db("SELECT * FROM books WHERE id = ?", [book_id], one=True)
    return {'status': 'ok', 'data': BookSerializer().serialize(data)}

@app.delete("/api/v1/books/{book_id}")
def delete_books(book_id):
    data = db.query_db("SELECT * FROM books WHERE id = ?", [book_id], one=True)
    if not data:
        raise HTTPException(status_code=404, detail="Book not found")
    db.query_db("DELETE FROM books WHERE id = ?", [book_id])
    return {'status': 'ok', 'data': BookSerializer().serialize(data)}

@app.post("/api/v1/parse/csv")
def parse_csv(file: UploadFile):
    try:
        content = StringIO(file.file.read().decode())
        csvreader = csv.reader(content, delimiter=',')
        data = [e for e in csvreader]
        data = BookSerializer().serialize(data)
    except Exception:
        raise HTTPException(status_code=500, detail="Invalid format file")
    return {'status': 'ok', 'data': data}

@app.on_event("shutdown")
def shutdown_event():
    db.destroy()